﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuickKill
{
    public partial class OptionsMenu : Form
    {
        public OptionsMenu()
        {
            InitializeComponent();
        }

        private void OptionsMenu_Load(object sender, EventArgs e)
        {
            if(Properties.Settings.Default.showMessage == true)
            {
                boxMessage.Checked = true;
            }

            else
            {
                boxMessage.Checked = false;
            }

            if (Properties.Settings.Default.confirmKill == true)
            {
                boxConfirmKill.Checked = true;
            }

            else
            {
                boxConfirmKill.Checked = false;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (boxConfirmKill.Checked == true)
            {
                Properties.Settings.Default.confirmKill = true;
            }

            else if (boxConfirmKill.Checked == false)
            {
                Properties.Settings.Default.confirmKill = false;
            }

            if(boxMessage.Checked == true)
            {
                Properties.Settings.Default.showMessage = true;
            }

            else if(boxMessage.Checked == false)
            {
                Properties.Settings.Default.showMessage = false;
            }

            Properties.Settings.Default.Save();
            this.Close();
        }
    }
}
